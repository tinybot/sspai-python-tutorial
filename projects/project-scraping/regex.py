# -*- coding:utf-8 -*-

import os
import re

import pandas as pd
import requests

url = "https://movie.douban.com/top250"
headers = {
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.92 Safari/537.36 Edg/81.0.416.53",
}

response = requests.get(url, headers=headers).text

# 获取 li 标签内容
li_pattern = r"<li>(.*?)</li>"
lis = re.findall(li_pattern, response, flags=re.MULTILINE | re.DOTALL)

# 遍历提取数据
titles = []
descs = []
rates = []
comments = []
for node in lis:
    title = re.findall(r'<span class="(title|other)">(.*)</span>', node)
    title = [t[1].replace("&nbsp;", "").strip() for t in title]
    titles.append("".join(title))

    desc = re.findall(r'<p class="">(.*?)</p>', node, flags=re.MULTILINE | re.DOTALL)
    desc = [d.strip() for d in desc]
    descs.extend(desc)

    rate = re.findall(
        r'<span class="rating_num" .*?>(.*?)</span>',
        node,
        flags=re.MULTILINE | re.DOTALL,
    )
    rate = [float(r) for r in rate]
    rates.extend(rate)

    comment = re.findall(r"<span>(\d+).*</span>", node, flags=re.MULTILINE | re.DOTALL)
    comment = [int(c) for c in comment]
    comments.extend(comment)


# 存储数据
info = pd.DataFrame(
    {"title": titles, "descs": descs, "rates": rates, "comment": comments}
)
data = pd.DataFrame(info)
path = os.path.expanduser("~/Desktop/re-result.csv")
data.to_csv(path, encoding = "UTF-8")
