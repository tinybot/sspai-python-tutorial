# -*- coding:utf-8 -*-

import os
from pprint import pprint

import requests
from pandas import json_normalize

url = "https://api.bilibili.com/x/space/arc/search"
headers = {
    "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.92 Safari/537.36 Edg/81.0.416.53",
    "origin": "https://space.bilibili.com",
    "cookie": "your-cookie", # 这里加上登录账号后自带的 cookie
}

# 构建参数字典
params = {
    "mid": "250858633",
    "ps": "30",
    "tid": "0",
    "pn": "2",
    "order": "pubdate",
    "jsonp": "jsonp",
}

# 向 API 接口发送请求
response = requests.get(url, headers=headers, params=params)

# 解析数据
data = response.json()
pprint(data, indent=2)

# 存储数据
table = json_normalize(data["data"]["list"]["vlist"])
table_path = os.path.expanduser("~/Desktop/api-result.csv")
table.to_csv(table_path)
