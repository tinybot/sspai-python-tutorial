# -*- coding:utf-8 -*-
import os
import re

import pandas as pd
import requests
from bs4 import BeautifulSoup

url = "https://movie.douban.com/top250"
headers = {
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.92 Safari/537.36 Edg/81.0.416.53",
}

response = requests.get(url, headers=headers).text

# 解析 HTML 页面
soup = BeautifulSoup(response, features="html.parser")

# 获取 li 标签
lis = soup.select("ol.grid_view li")

# 遍历 li 标签
titles = []
descs = []
rates = []
comments = []

for li in lis:

    title = li.select("div.hd > a > span")
    titles.append("".join([content.text for content in title]))

    desc = li.select("div.bd > p:nth-child(1)")
    desc = [content.text.strip() for content in desc]
    descs.extend(desc)

    rate = li.select("div.star span.rating_num")
    rate = [float(content.text) for content in rate]
    rates.extend(rate)

    comment = li.select("div.star span:last-child")
    comment = [int(re.findall(r"\d+", content.text)[0]) for content in comment]
    comments.extend(comment)


# 存储数据
info = pd.DataFrame(
  {"title": titles, "descs": descs, "rates": rates, "comment": comments}
)

path = os.path.expanduser("~/Desktop/css-result.csv")
info.to_csv(path, encoding = "UTF-8")
