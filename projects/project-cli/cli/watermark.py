# -*- coding:utf-8 -*-

import os
import traceback

from PIL import Image

__all__ = ["watermark"]

def mark(image_path, watermark_path, output_path, pos):
    # 打开待加水印图片，并获取图片的长宽数值
    img = Image.open(image_path)
    img_width, img_height = img.size

    # 打开水印图片，由于水印图片较小，所以打开后对其大小进行五倍的缩放操作，再获取最终的缩放数值
    wm = Image.open(watermark_path)
    wm = wm.resize(tuple(map(lambda param: int(param * 5), wm.size)))
    wm_width, wm_height = wm.size
    print(f"watermarker size is: {wm.size}")

    # 设定水印摆放位置的元组对字典，用以代替多层 if-else
    positions = {
        "topleft": (0, 0),
        "topright": ((img_width - wm_width) - 100, 0),
        "bottomleft": (0, (img_height - wm_height) + 100),
        "bottomright": ((img_width - wm_width) - 100, (img_height - wm_height) - 100),
        "center": (int((img_width - wm_width) / 2), int((img_height - wm_height) / 2))
    }

    try:
        # 使用字典的 get 方法获取键值，如果不存在着返回一个 KeyError 对象
        wm_position = positions.get(pos, KeyError)

        # 创建一个新图层，图层大小与原始图片的大小一致
        layer = Image.new(mode = "RGBA", size = img.size)

        # 将水印贴到这个图层上，并使用 mask 参数来将水印设置为透明的遮罩
        layer.paste(wm, wm_position, mask = wm)

        # 将带有水印的图层和原始图片组合到一起，composite 操作等价于上面 paste 操作，并最后将其保存
        marked_img = Image.composite(layer, img, layer)
        marked_img.save(output_path)
    except Exception as e:
        traceback.print_exc()


def watermark(dir, watermark_path, pos = "topleft"):
    os.chdir(dir)
    imgs = os.listdir()
    marked_dir_path = os.path.join(dir, "marked")
    if not os.path.exists(marked_dir_path):
        os.mkdir(marked_dir_path)

    for img in imgs:
        if (not os.path.isdir(img) and
            not img.startswith(".") and
            os.path.splitext(img)[-1] in [".jpg", ".png", ".bmp"]
        ):
            marked_file_path = os.path.join(marked_dir_path, "marked-" + img)
            mark(image_path = img,
                 watermark_path = watermark_path,
                 output_path = marked_file_path,
                 pos = pos)
        else:
            continue


if __name__ == "__main__":
    watermark(
        dir = "/Users/Bobot/Desktop/testimgs",
        watermark_path = "/Users/Bobot/Desktop/sspai_logo.png",
        pos = "bottomright"
    )
