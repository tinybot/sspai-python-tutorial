# -*- coding:utf-8 -*-

import click

from cli.formatting import formatting_files
from cli.watermark import watermark


@click.group()
def cli():
    """我的命令行工具：基于 Click 库"""
    pass


@cli.command(help = "格式化文档")
@click.option(
    "--path", "-p",
    type = click.Path(exists = True),
    help = "存放待格式化文本的文件夹路径"
)
def fmt(path):
    click.echo("fmt is called.")
    formatting_files(path)


@cli.command(help = "批量添加水印")
@click.option(
    "--dir", "-d",
    required = True,
    type = click.Path(exists = True),
    help = "存放待添加水印的图片的文件夹"
)
@click.option(
    "--mark", "-m",
    required = True,
    type = click.Path(exists = True),
    help = "水印图片文件路径",
)
@click.option(
    "--pos", "-p",
    default = "topleft",
    type = click.Choice(["topleft", "topright", "bottomleft", "bottomright", "center"],
                        case_sensitive = False),
    help = "水印摆放位置"
)
def marked(dir, mark, pos):
    click.echo("watermark is called.")
    watermark(
        dir = dir,
        watermark_path = mark,
        pos = pos
    )


if __name__ == "__main__":
    cli()
