# -*- coding:utf-8 -*-

import os

import pangu

__all__ = ["formatting_files"]

def formatting_files(path):
    os.chdir(path)
    files = os.listdir()
    formatting_dir_path = os.path.join(path, "spacing")
    if not os.path.exists(formatting_dir_path):
        os.mkdir(formatting_dir_path)

    for file in files:
        if (not os.path.isdir(file) and
            not file.startswith(".") and
            os.path.splitext(file)[-1] in [".md", ".txt", ".rtf"]
        ):
            formatting_file_path = os.path.join(formatting_dir_path, "formatted-" + file)
            with open(formatting_file_path, mode = "w+") as f:
                f.write(pangu.spacing_file(file))
                print(f"{file} file has been formatted.")
        else:
            continue


if __name__ == '__main__':
    pass
    # Input your dir path
    # formatting_files(path)
