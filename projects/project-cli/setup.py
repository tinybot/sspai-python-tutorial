# -*- coding:utf-8 -*-

from setuptools import find_packages, setup

setup(
    name = 'my-cli',
    version = '0.1',
    packages= find_packages(),
    include_package_data = True,
    install_requires = [
        'click',
        'pillow',
        'pangu'
    ],
    entry_points = '''
        [console_scripts]
        cli=cli.main:cli
    ''',
)
