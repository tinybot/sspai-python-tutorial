# coding:utf-8

import click


@click.command()
@click.argument("string")
@click.option("--delete", "-d", default=" ", help="the char to be deleted.")
def tr(string, delete):
    click.echo(string.replace(delete, ""))

if __name__ == "__main__":
    tr()
