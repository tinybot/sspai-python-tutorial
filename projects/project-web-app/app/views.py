# -*- coding:utf-8 -*-

from flask import Blueprint, jsonify, redirect, render_template, request

from app.extensions import db
from app.models import Task

bp = Blueprint("bp", __name__)


def result(message, status_code=1, data=None):
    if data is None:
        return jsonify(message=message, status_code=status_code)
    else:
        return jsonify(message=message, data=data, status_code=status_code)


@bp.route("/", methods=["GET", "POST"])
def index():
    return render_template("index.html")


@bp.route("/task/add", methods=["POST"])
def add_task():
    """add new task"""

    content = request.form.get("content")
    priority = request.form.get("priority")
    task = Task(content=content)
    if priority is not None:
        task.priority = priority
    db.session.add(task)
    db.session.commit()
    return redirect("/")


@bp.route("/task", methods=["GET"])
def query_task_all():
    """query all tasks"""

    tasks = Task.query.all()
    data = [task.to_json() for task in tasks]
    return result(message="查询成功", data=data)


@bp.route("/task/edit", methods=["POST"])
def edit_task():
    """edit task by id"""

    data = request.get_json()

    id = data.get("id")
    task = Task.query.get(id)
    if task is None:
        return result(status_code=0, message="修改失败，找不到对应任务")

    is_done = data.get("is_done")

    if is_done is not None:
        task.is_done = is_done

    db.session.commit()
    return result(status_code=1, message="任务修改成功")


@bp.route("/task/delete/<int:id>", methods=["DELETE"])
def delete_task(id):
    """delete task by id"""

    task = Task.query.get(id)
    if task is None:
        return result(status_code=0, message="删除失败，找不到对应任务")
    db.session.delete(task)
    db.session.commit()
    return result(status_code=1, message="任务删除成功")
