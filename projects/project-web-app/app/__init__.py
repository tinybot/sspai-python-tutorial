# -*- coding:utf-8 -*-

from flask import Flask

from app.config import FlaskAppConfig
from app.extensions import cors, db


def create_app():
    app = Flask(__name__)
    app.config.from_object(FlaskAppConfig)

    db.init_app(app)
    cors.init_app(app)

    from app.views import bp

    app.register_blueprint(bp)

    return app


myapp = create_app()


@myapp.before_first_request
def init_db():
    db.create_all()

    from .models import Task

    if not Task.query.first():
        tasks = [
            Task(content="吃饭", priority=3),
            Task(content="睡觉", priority=2),
            Task(content="打豆豆", priority=1),
        ]
        db.session.add_all(tasks)
        db.session.commit()


if __name__ == "__main__":
    myapp.run()
