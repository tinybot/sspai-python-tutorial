# -*- coding:utf-8 -*-

from app.extensions import db


class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False, comment="任务编号")
    content = db.Column(db.String(50), comment="待办事项")
    priority = db.Column(db.Integer, default=1, comment="优先级")
    is_done = db.Column(db.Integer, default=0, comment="是否完成")

    def to_json(self):
        return {
            "id": self.id,
            "content": self.content,
            "priority": self.priority,
            "is_done": self.is_done,
        }
