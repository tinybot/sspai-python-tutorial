# -*- coding:utf-8 -*-

import os


class FlaskAppConfig:
    """Configure Flask App with Python Class Object."""

    SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(
        os.path.dirname(__file__), "db.sqlite3"
    )
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
