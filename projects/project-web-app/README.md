# 使用

1. 激活虚拟环境，并切换到 `web-flask-todo-app` 目录中

2. 使用 `pip` 安装 `requirements.txt` 文件中的依赖库

```
pip install -r requirements.txt
```

3. 在终端运行 Flask

```
flask run
```

之后在浏览器中输入 `127.0.0.1:5000` 就可以看到整个项目启动之后的效果。