import multiprocessing as mp


def compute(x):
    return x**2

if __name__ == '__main__':
    p = mp.Pool()
    result = p.map(compute, range(10))
    print(result)
