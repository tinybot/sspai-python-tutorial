#!usr/bin/env python3
# -*- coding:utf-8 -*-

import random

number = random.randint(1, 20) 

while True: 
    guess = int(input("请输入数字：")) 
    if guess == number: 
        print("Bingo!恭喜你猜对了，数字是: %s" % number) 
        break 
    elif guess < number: 
        print("Oops!你好像猜小了...请再猜一次") 
    else: 
        print("Aha, 好像猜大了...再猜一次吧！") 
