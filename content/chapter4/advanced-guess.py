#!usr/bin/env python3
# -*- coding:utf-8 -*-

import random
import sys
from threading import Timer

number = random.randint(1, 20)
timeout = 30 

def main(): 
    n = 10 
    t = Timer(timeout, print, ["\n不好意思，时间用尽，Game Over！"]) 
    t.start() 
    while n != 0:  
        print("目前剩余次数：%s" % n)
        try: 
            guess = int(input("请输入数字："))
        except ValueError as e: 
            sys.exit(0) 
        n -= 1  
        if guess == number:
            print("Bingo!恭喜你猜对了，数字是: %s" % number)
            break
        elif guess < number:
            print("Oops!你好像猜小了...请再猜一次")
        else:
            print("Aha, 好像猜大了...再猜一次吧！")
        if n == 0:
            print("次数耗尽，游戏结束")
            break
    t.cancel()

if __name__ == "__main__": 
    main()
