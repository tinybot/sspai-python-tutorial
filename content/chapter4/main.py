# 二、
a,b = 10, 20 
print(a & b) 
print(a and b) 


# 三、

a, b=10, 20 #1
if a>b: #2
    print('a>b') #3
else:
    print('a<b') #4

a, b=10, 10
if a==b:
    print('a 等于 b') #返回 "a 等于 b"

a,b,c=10,20,30 #1
if not ((a!=b) and (a>c)): #2
    print('a!=b且a<c') #3
else:
    print('a==b或a>=c') #4

# 四、
money = 4000
if money>10000:
    print('money>10000')
else:
    if money>5000:
        print('money>5000')
    else:
        if money>2500:
            print('money>2500') 
        else:
            print('money<=2500')

money = 4000
if money>10000:
    print('money>10000')
elif money>5000:
    print('money>5000') 
elif money>2500:
    print('money>2500') #返回该结果
else:
    print('money<=2500') 

# 五、

input_money=2000
def guess_money(money):
    if money>10000:
        print('money>10000')
    if money>5000:
        print('money>5000') 
    if money<=2500:  #直接进入到该执行流程
        print('money<=2500')
guess_money(input_money)
