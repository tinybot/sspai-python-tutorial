import os
import random
import re
import sys
from urllib.request import urlopen, urlretrieve

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QApplication, QGraphicsScene, QWidget

WALLPAPERS = [
    'https://tva1.sinaimg.cn/large/00831rSTgy1gd49h3lj8tj31hc0u01lb.jpg',
    'https://tva1.sinaimg.cn/large/00831rSTgy1gd49gxvs5uj31hc0u0qvh.jpg',
    'https://tva1.sinaimg.cn/large/00831rSTgy1gd49gxvs5uj31hc0u0qvh.jpg',
    'https://tva1.sinaimg.cn/large/00831rSTgy1gd49gswwzfj31hc0u07wv.jpg',
    'https://tva1.sinaimg.cn/large/00831rSTgy1gd49gndxthj31hc0u04r1.jpg'
]


class Wallpaper(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("壁纸获取器 1.0（By 100gle）")
        self.initUI()
        self.reloadButton.clicked.connect(self.reload)
        self.saveButton.clicked.connect(self.save)
        self.reload()

    def initUI(self):
        self.reloadButton = QtWidgets.QPushButton(self)
        self.reloadButton.setGeometry(QtCore.QRect(50, 520, 400, 100))
        self.reloadButton.setObjectName("reloadButton")
        self.previewer = QtWidgets.QGraphicsView(self)
        self.previewer.setGeometry(QtCore.QRect(30, 30, 960, 480))
        self.previewer.setObjectName("previewer")
        self.saveButton = QtWidgets.QPushButton(self)
        self.saveButton.setGeometry(QtCore.QRect(550, 520, 400, 100))
        self.saveButton.setObjectName("saveButton")
        self.reloadButton.setText("刷新")
        self.saveButton.setText("保存")

    @property
    def display(self):
        url = random.choice(WALLPAPERS)
        return self.displayImage(url)

    def displayImage(self, url):
        self.url = url
        data = urlopen(url).read()

        image = QImage()
        image.loadFromData(data)

        scene = QGraphicsScene()
        scene.addPixmap(QPixmap(image).scaled(960, 480))
        return scene

    def reload(self):
        self.previewer.setScene(self.display)

    def save(self):
        name, suffix = re.search(r"large/(.*)\.(.*)", self.url).groups()
        filename = os.path.expanduser("~/Desktop/{}.{}".format(name, suffix))
        urlretrieve(url = self.url, filename = filename)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    paper = Wallpaper()
    paper.show()
    sys.exit(app.exec_())
