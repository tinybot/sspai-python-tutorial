# 一、
## 1.2
class Phone(object):

    def call(self, telnumber):
        print(f"{telnumber} is calling...")

    def browse(self, web):
        print(f"{web} is loading...")
    
    def playgame(self):
        print(f"now we can support playing games!")

class AndroidPhone(Phone):
    
    def __init__(self):
        self.os = 'Android'

class iPhone(Phone):

    def __init__(self):
        self.os = 'iOS'


android = AndroidPhone()
android.call(12345)
iphone = iPhone()
iphone.browse("https://www.sspai.com")

android = AndroidPhone()
android.playgame()
iphone = iPhone()
iphone.playgame()


# 二、

class human:
    eyes = 2
    hands = 2
    ears = 2

man = human()
print(man) 

print(man.eyes) # 返回 2
print(man.hands) # 返回 2
print(man.ears) # 返回 2

class human:
    eyes = 2
    hands = 2
    ears = 2

    def __init__(self, gender):
        self.gender = gender

    def talk(self):
        print('Hello! my gender is {}'.format(self.gender))

man = human(gender='male')
women = human(gender='female')

print(man.gender) # 返回 male
print(women.gender) # 返回 female

man.talk() #返回 Hello! my gender is male
women.talk() #返回 Hello! my gender is female


class human:
    eyes = 2
    hands = 2
    ears = 2
    def __init__(gender):
        self.gender = gender
        
man = human('male')


class human:
    eyes = 2
    hands = 2
    ears = 2
    def __init__(self, gender):
        self.gender = gender
        
man = human('male')


class human:
    eyes = 2
    hands = 2
    ears = 2
    def __init__(self, gender):
        self.gender = gender
    def get_self(self):
        print(self)
        
man = human('male')
man.get_self() 
print(man)


class human:
    eyes = 2
    hands = 2
    ears = 2
    def __init__(a, gender):
        a.gender = gender
    def get_self(a):
        print(a)
        
man = human('male')
man.get_self() 


# 三、
## 3.1

class human:
    eyes=2
    hands=2
    ears=2

    def __init__(self, gender, hair):
        self.gender = gender
        self.hair = hair

man=human(gender='male', hair='short')
women=human(gender='female', hair='long')

#公有属性
print(man.eyes, women.eyes, sep='\n')
print(man.hands, women.hands, sep='\n')
print(man.ears, women.ears, sep='\n')

#对象本身所具有的属性
print(man.gender, man.hair, sep='\n') #返回 male short
print(women.gender, women.hair, sep='\n') #返回 female long


class man(human):
    def __init__(self, age, type):
        self.age=age
        self.type=type

boy=man(age=10, type='young')
grandfather=man(age=70, type='old')

print(boy.hands, grandfather.eyes, sep='\n') #返回 2
print(boy.type) # 返回 young
print(grandfather.age) # 返回70
print(boy.gender)


class human:
    eyes=2
    hands=2
    ears=2
    def __init__(self, gender, hair):
        self.gender = gender
        self.hair = hair
        print('父类属性：',self.gender, self.hair)

class man(human): 
    def get_attrs(self):
        print('子类属性：', self.gender, self.hair)

boy = man('male', 'black') #返回父类属性： black male
boy.get_attrs() #子类属性： black male


## 3.2

class Phone(object):
    
    def __init__(self, name):
        if name is 'iPhone':
            self.os = 'iOS'
        else:
            self.os = 'Android'

    def call(self, number):
        print('Calling %s' %number)

    def photo(self):
        print('Your Phone has taken a photo.')

iPhone = Phone('iPhone')
print(iPhone.os) #返回 iOS
iPhone.call('10086') #返回Calling 10086
iPhone.photo() #返回Your Phone has taken a photo.


class iphone(Phone):
    def __init__(self, name):
        self.name = name


ip11pro = iphone(name='iPhone 11 Pro')
print(ip11pro.name) # 返回iPhone 11 Pro
ip11pro.call('10000') # 返回 Calling 10000
ip11pro.photo() # 返回 Your Phone has taken a photo.


# 四、
## 4.1

import datetime


class clock:

    def __init__(self):
        pass
    
    def report_now(self):
        now = datetime.datetime.now()
        return now.strftime(r'%Y-%m-%d %H:%M:%S')
        
clock_A = clock()
clock_A.report_now() #返回当天的日期时间


class clock:

    def __init__(self):
        pass
    
    @property
    def report_now(self):
        now = datetime.datetime.now()
        return now.strftime(r'%Y-%m-%d %H:%M:%S')
        
clock_A = clock()
clock_A.report_now #返回当前日期时间


## 4.2
class Phone(object):
    def __init__(self, name):
        if name is 'iPhone':
            self.os = 'iOS'
        else:
            self.os = 'Android'

    def call(number):
        print('Calling %s' %number)

    @staticmethod
    def photo():
        print('Your Phone has taken a photo.')

iphone = Phone('iPhone')
iphone.call('10086')
iphone.photo()

Phone.photo() 

## 4.3

class Phone(object):
    
    def __init__(self, name):
        if name is 'iPhone':
            self.os = 'iOS'
        else:
            self.os = 'Android'

    @staticmethod
    def call(number):
        print('Calling %s' %number)

    def photo(self):
        print('Your Phone has taken a photo.')

    @classmethod
    def SOS(cls):
        return cls.call('110')

iphone=Phone('iPhone')
iphone.SOS() #结果返回 Calling 110
