
# 一、
def square(x):
    result = x**2
    return result

square(4) #返回16


def count(num):
    for n in range(num):
        yield n

def def_count(num):
    for n in range(num):
        return n

print(count(5)) # 返回 generator
print(def_count(5)) # 返回 0

def def_count(num):
    for n in range(num):
        print(n)

def_count(5) #结果返回 0,1,2,3,4

for n in range(5): # 结果返回 0,1,2,3,4
    n


def def_count(num):
    for n in range(num):
        n
def_count(5) #什么都没有返回

print(def_count(5)) #返回 None


# 二、
def count(a, b, *args, **kwargs):
    return a ** b

## 2.1
count(a=2, b=3) #返回 8
count(2, 3) #等价于 count(a=2,b=3)

## 2.2
def count(a, b=2, *args, **kwargs):
    return a ** b

count(10) #返回 100
count(a=10, b=3) #返回 1000

## 2.3

def count(*args):
    print(args)

params = [2,3,4,5]
count(*params) #返回 (2,3,4,5)

def count(a, b, *args, **kwargs):
    print('a:', a)
    print('b:', b)
    print('args:', args)


params = [2,4,3,4,5,6,6]
count(*params)

def count(a, b, c, *args, **kwargs):
    print('a:', a)
    print('b:', b)
    print('c:', c)
    print('args:', args)

params = [2,4,3,4,5,6,6]
count(*params)
count(a=10, *params)
count(10, *params)

## 2.4

def count(a, b):
    return a**b

count(a=10, b=2) #返回100

kw = dict(a=10, b=2)
count(**kw) #返回 100

def count(**kwargs):
    print(kwargs)

count(**kw) #返回{'a':10, 'b':20}

def count(a,b,**kwargs):
    print('a:', a)
    print('b:', b)
    print('kwargs:', kwargs)

kw = dict(a=10,b=2,c=10,d=40)
count(**kw)

def count(**kwargs):
    print(kwargs)

kw = {1,2,3}
count(**kw) # 报错

# 三、
## 3.1 
numbers = [1,2,3]
result = [n for n in map(lambda x: x**2, numbers)]
print(result) #返回[1,4,9]

numbers = [1,2,3]
def square(x):
    return x**2

result = [square(x) for x in numbers]
print(result) #返回[1,4,9]

square = lambda x: x**2
square(3) #返回值9

## 3.2

def add(x, y):
    return x + y

def times(x, y):
    return x * y

def ufunc(function, iterable):
    iterator = iter(iterable)
    value = next(iterator)
    for num in iterator:
        value = function(value, num)
    return value

numbers = [1,2,3,4,5,6,7,8,9]
print(ufunc(add, numbers)) # 累加
print(ufunc(times, numbers)) # 累乘

print(ufunc(lambda x,y: x+y, numbers))
print(ufunc(lambda x,y: x*y, numbers))


numbers = [1,2,3,4,5,6,7,8,9]

# 求 1-9 每个数的平方
square = list(map(lambda x: x**2, numbers))
print(square) # 返回 [1, 4, 9, 16, 25, 36, 49, 64, 81]


odds = list(filter(lambda x: x%2!=0, numbers)) # [1,3,5,7,9]
evens = list(filter(lambda x: x%2==0, numbers)) # [2,4,6,8]
print(f"1-9中奇数为：{odds}，偶数为：{evens}")


# 四、
def sayhello():
    print('Hey!')

def decorator(func):
    print('function name:', func.__name__)
    print('function is running……')
    f = func()
    return f

decorator(sayhello)


def decorator(func):
    def wrapper():
        print('function name:', func.__name__)
        print('function is running……')
        func()
    return wrapper

@decorator
def sayhello():
    print('Hello, world!')

sayhello() #返回同样的结果


def decorator(cls):
    def wrapper(*args):
        print("哔哩哔哩干杯！×1")
        print("哔哩哔哩干杯！×2")
        return cls(*args)
    return wrapper

@decorator
class uper(object):
    def __init__(self, slogon):
        self.slogon = slogon

X = uper('奥利给！')
