#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import os
import shutil
from glob import glob

#修改为整理文件的路径
os.chdir(os.path.expanduser('~/Desktop/06-file-classify'))

#获取文件后缀，作为文件分类的依据
dir_files = set(map(lambda f: f.split(sep='.')[1] ,os.listdir()))
print(dir_files)

#定义归类操作
def move_file(suffix):
    for single_file in glob('*.{}'.format(suffix)):
        shutil.move(single_file, class_dir)
        print(f'file:{single_file} moved to --> dir: {class_dir}')

#循环遍历所有文件
for suffix in dir_files:
    class_dir = '{}'.format(suffix) 
    if not os.path.exists(class_dir):
        os.mkdir(suffix)
        move_file(suffix)
    else:
        move_file(suffix)
