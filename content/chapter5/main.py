
## 1.1

loop_list = [1,2,3,4,5]
for i in loop_list:
    print(i)


loop_dict = dict.fromkeys(list('abc'), 10)
for key, value in loop_dict.items():
    print("key {}: value {}".format(key, value))


loop_str = 'abcde'
for i in loop_str:
    print(i)

for i in range(5):
    print(i)

from collections.abc import Iterable

loop_str = 'abcde'
print(isinstance(loop_str, (Iterable))) #返回 True

## 1.2

numbers = [1,2,3]
iter_numbers = iter(numbers)
next(iter_numbers)
next(iter_numbers)
next(iter_numbers)
next(iter_numbers) #抛出StopIteration异常

## 1.3

def count(num):
    for n in range(num):
        yield n

c = count(5)
print(c) #返回该迭代器的内存地址
type(c) #返回generator

for n in c:
    print(n)


# 二、

for i in range(5):
    print(i)

for i in range(1,10):
    for j in range(1,10):
        print('{} * {} = {}'.format(i, j, i*j))

loop_dict = dict.fromkeys(list('abc'), 1)
for n, k in enumerate(loop_dict.keys()):
    print(n,k)

odds = []
evens = []
for i in range(1, 11):
    if i % 2 == 0:
        evens.append(i)
    else:
        odds.append(i)

print('1-10 内的奇数是：%s' %odds) #返回[1, 3, 5, 7, 9]
print('1-10 内的偶数是：%s' %evens) #返回[2, 4, 6, 8, 10]

# 三、

number = 0 
while number <5:
    number += 1
    print('你的数字小于 5，已经为其自动加 1。现在数字是%d' %number)

print(number)

# 四、
## 4.1

for i in range(1, 11):
    if i % 2 ==0:
        continue
    print('当前值为: %s' %i)

count = 0
while count <10:
    count += 1
    if count % 2==0:
        continue
    print('当前值：%s' %count)

i = 0
while True:
    i += 1
    if i>5:
        print('当前i=%s'%i) #i=6停止循环
        break

count = 0
for i in range(6):
   count += i 
   if count >5:
       print('当前值:%s' %count) #i=6停止循环
       break

# 五、

my_list = []
for i in range(10):
    my_list.append(i)

my_list

def myGenerator():
    for i in range(10):
        yield i

my_generator = myGenerator()
my_generator

my_list = [i for i in range(10)]
my_generator = (i for i in range(10))
print(type(my_list)) #返回 <class 'list'>
print(type(my_generator)) #返回 <class 'generator'>

my_list = [x for x in range(11) if x % 2 != 0]
print(my_list) #输出[1,3,5,7,9]


data_list = [[1,2,3], [4,5,6]]
my_list = [x for lst in data_list 
             for x in lst if x % 2 != 0] #返回[1,3,5]
my_list


data_list = [[1,2,3], [4,5,6]]
my_list = []
for lst in data_list:
    for x in lst:
        if x%2 != 0:
            my_list.append(x)

print(my_list) #返回[1,3,5]
