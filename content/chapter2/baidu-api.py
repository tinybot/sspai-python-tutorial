#!usr/bin/env python3
# -*- coding:utf-8 -*-

from pprint import pprint

from pandas.io.json import json_normalize

from aip import AipOcr

""" 替换成你的应用中的 APPID AK SK """
APP_ID = '********'
API_KEY = '************************'
SECRET_KEY = '********************************'
client = AipOcr(APP_ID, API_KEY, SECRET_KEY)

# 读取图片
def get_file_content(filePath):
    with open(filePath, 'rb') as fp:
        return fp.read()
image = get_file_content(r'desc.jpeg') # 修改成示例图片所在的绝对路径

len(image) # 查看图片二进制长度
result = client.basicGeneral(image) # 调用接口
pprint(result)

text = json_normalize(result['words_result']) # 结构化返回数据
text.to_csv(r'rslt.txt', index=False, 
            encoding='utf-8-sig') # 可将导出路径修改成你自己所指定的路径
