# 一、
myList = [1+1, '1+1', [1,2,3,4,5]]
print(myList)


# 二、
type(1+1) #int
type(1.0) #float
type(3.14159267) #float
type(1+4j) #complex
type(True) #bool
type(False) #bool


# 三、
# 3.1
# 方括号创建列表、圆括号创建元组
myList1 = [1,2,3,4]
myTuple1 = (1,2,3,4)

# list 关键字创建列表，tuple 创建元组
myList2 = list('abcde')
myTuple2 = tuple('abcde')


box = ['one', 'two', 'three', 'four', 'five']
box[0]
box[0:3]


# 3.1.3
apple = ['iPhone', 'iPad', 'MacBook', 'Apple Watch', 
         'Mac Pro', 'iMac', 'Mac mini', 'iMac Pro']

apple.append('iPod') # 1
apple.extend('iPhone 11 Pro') #2
apple.remove('iMac Pro') #3
apple.pop() #默认删除最后一个 #4
apple.pop(2) #弹出第3个元素 
apple.index('Mac mini') #5


# 3.2 
info = {
    'name': '100gle',
    'personal_info': {
        'slogen': '自律即自由',
        'gender': 'male'
    }, 
    100: 'Hello, world!',
    100: 'Hello, sspai!',
    'money': [1000, 10000, 100000]
}

from pprint import pprint

print(info)

info['name']
info[100]

# 3.2.4

info = {
    'name': '100gle', 
    'telephone': '021-XXXX-XXXX', 
    'gender': 'male', 
    'email': 'xiaoyue.lin@outlook.com'
}

info.keys() #1
info.values() #2

info2 = dict(id=0) #3
info.update(info2) #3


# 3.3.1
a = {}
type(a)

b = {1,2}
type(b)

# 3.3.2 

set1 = {'apple', 'google', 'microsoft'}
set2 = {'apple', 'huawei', 'oneplus'}

set1.add('Amazon') #1
set1.intersection(set2) #2
set1.union(set2) #3
set1.difference(set2) #4
