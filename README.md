本仓库为 [少数派](https://sspai.com/) 平台 [《Python 自学手册》](https://sspai.com/series/148) 教程内容的相关源码、以及素材仓库，教程内的所有代码及案例都可通过对应的目录找到。

在使用过程中如有任何问题欢迎在少数派私信我，或者在本仓库上面的 Issues 页面中提问。

# 目录说明

**content**

主要存放的正文章节内容源码及其实践案例，读者也根据文章中的内容逐步实践

**projects**

主要存放综合案例部分的源码

# 使用说明

读者可以根据不同章节所属部分找到对应的目录，然后使用推荐的 VS Code 将目录作为一个工作区（Workspace）打开。

比如我们正在学习第七章的内容，则找到 `content\chapter7` 这个目录，然后将该目录拖入 VS Code 中即可。

![](img/usage.gif)

# LICENSE

```
MIT License

Copyright (c) 2019-2022 100gle

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```